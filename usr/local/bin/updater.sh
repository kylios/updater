#!/bin/bash

date | tee /etc/sys-updates
pacman -Sy | tee -a /etc/sys-updates

TO_UPDATE=$(pacman -Qu)

if [ ! -z "$TO_UPDATE" ]; then
    echo $TO_UPDATE | tee -a /etc/sys-updates
    echo | tee -a /etc/sys-updates
    echo "Updates needed. Run `sudo pacman -Su` to update." | tee -a /etc/sys-updates
fi

