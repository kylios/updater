Updater
=====

A simple script, systemd unit file, and timer to check for and report system updates daily.

# Requirements

This systemd unit file and script is intended to be run in Arch Linux, since it invokes arch's `pacman` package manager. You should be able to use it with any other linux distribution by substituting the `pacman` commands with the equivalent commands for your system's package manager.

# Installation

Copy the contents of this directory into `/`. So you should end up with:

```
/
└── usr
    ├── lib
    │   └── systemd
    │       └── system
    │           ├── updater.service
    │           └── updater.timer
    └── local
        └── bin
            └── updater.sh
```

Make sure that each file is owned by `root`, and that `/usr/local/bin/updater.sh` has `+x` permissions set.

# Usage

First test that everything is working:

```
sudo systemctl start updater.service
```

This should take a moment. If you run `journalctl -f`, you should see the output from `updater.sh` printed to the journal. Upon completion, the file `/etc/sys-updates` should be present.

Now enable the timer:

```
sudo systemctl enable updater.timer
```

This timer will run the updater script daily. You can change the frequency by editing the `updater.timer` file. Regardless, each time it runs, the `/etc/sys-updates` file will be overwritten.

If you would like to see whether updates are necessary when you open a new terminal window, simple add the following lines to your `.bashrc` file (or equivalent):

```
if [ -f /etc/sys-updates ]; then
    cat /etc/sys-updates
fi
```
